package ru.gb.espressoapp_gb.tests

import androidx.test.ext.junit.rules.ActivityScenarioRule
import androidx.test.ext.junit.runners.AndroidJUnit4
import org.junit.Rule
import org.junit.Test
import org.junit.runner.RunWith
import ru.gb.espressoapp_gb.SettingsActivity
import ru.gb.espressoapp_gb.pages.SettingsPage

@RunWith(AndroidJUnit4::class)
class SettingsTest {

    @get:Rule
    val activityRule = ActivityScenarioRule(SettingsActivity::class.java)

    @Test
    fun checkInputSignatureTextIsDisplayed(){
        val signature = "test"
        SettingsPage().setSignature(signature)
            .clickOkButton()
            .checkSignatureTextIsDisplayed(signature)
            .clearSignature()
            .clickOkButton()

    }

    @Test
    fun checkInputSignatureTextIsNotDisplayedIfCanceled(){
        val signature = "TEST"
        SettingsPage().setSignature(signature)
            .clickCancelButton()
            .checkSignatureTextIsNotDisplayed()
    }
}


