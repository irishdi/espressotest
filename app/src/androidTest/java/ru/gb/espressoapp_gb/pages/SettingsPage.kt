package ru.gb.espressoapp_gb.pages

import android.R
import androidx.test.espresso.Espresso.onView
import androidx.test.espresso.action.ViewActions.*
import androidx.test.espresso.assertion.ViewAssertions.matches
import androidx.test.espresso.matcher.ViewMatchers.*
import org.hamcrest.CoreMatchers.allOf


class SettingsPage {

    fun setSignature(signature: String) :SettingsPage{
        onView(withChild(withText("Your signature")))
            .perform(click())
//        onView(withClassName(containsString("AppCompatEditText"))).perform(typeText("test"))
        onView(withId(R.id.edit)).perform(typeText(signature))
        return this
    }

    fun clickOkButton():SettingsPage{
        onView(withId(R.id.button1)).perform(click())
        return this

    }

    fun checkSignatureTextIsDisplayed(signature: String):SettingsPage{
        onView(allOf(
                withId(R.id.summary),
                withText(signature)
            )
        ).check(matches(isDisplayed()))
        return this
    }

    fun clearSignature():SettingsPage{
        onView(withChild(withText("Your signature")))
            .perform(click())
        onView(withId(R.id.edit)).perform(clearText())
        return this
    }

    fun clickCancelButton():SettingsPage{
        onView(withId(R.id.button2)).perform(click())
        return this
    }

    fun checkSignatureTextIsNotDisplayed():SettingsPage{
        val s = "Not set"
        onView(allOf(
            withId(R.id.summary), withText(s))).check(
            matches(
                isDisplayed()))
        return this
    }
}